# -*- coding: utf-8 -*-

import random


class Game:
    def __init__(self):
        self.numbers = {}
        self.current_position = 0

    def print_board(self):
        numbers = self.numbers
        print('|%s - %s - %s - %s|' % (numbers[1], numbers[2], numbers[3], numbers[4]))
        print('|%s - %s - %s - %s|' % (numbers[5], numbers[6], numbers[7], numbers[8]))
        print('|%s - %s - %s - %s|' % (numbers[9], numbers[10], numbers[11], numbers[12]))
        print('|%s - %s - %s - %s|' % (numbers[13], numbers[14], numbers[15], numbers[16]))

    def make_board(self):
        for i in range(1, 17):
            self.numbers[i] = i

        tmp = 0
        for key, value in self.numbers.items():
            if value == 16:
                self.numbers[key] = 0
                tmp = key
                break

        self.current_position = tmp

        # Перемешиваем доску
        for _ in range(16):
            free_cells = self.valid_moves()
            temp_list = []
            for i in free_cells:
                temp_list.append(i)
            self.change(temp_list[random.randint(0, len(temp_list) - 1)])

    def valid_moves(self):
        pos = self.current_position

        if pos in [6, 7, 10, 11]:
            return self.numbers[pos - 4], self.numbers[pos - 1], self.numbers[pos + 1], self.numbers[pos + 4]
        elif pos in [5, 9]:
            return self.numbers[pos - 4], self.numbers[pos + 4], self.numbers[pos + 1]
        elif pos in [8, 12]:
            return self.numbers[pos - 4], self.numbers[pos + 4], self.numbers[pos - 1]
        elif pos in [2, 3]:
            return self.numbers[pos - 1], self.numbers[pos + 1], self.numbers[pos + 4]
        elif pos in [14, 15]:
            return self.numbers[pos - 1], self.numbers[pos + 1], self.numbers[pos - 4]
        elif pos == 1:
            return self.numbers[pos + 1], self.numbers[pos + 4]
        elif pos == 4:
            return self.numbers[pos - 1], self.numbers[pos + 4]
        elif pos == 13:
            return self.numbers[pos + 1], self.numbers[pos - 4]
        elif pos == 16:
            return self.numbers[pos - 1], self.numbers[pos - 4]

    def change(self, to_position):
        from_position = self.current_position
        for key, value in self.numbers.items():
            if value == to_position:
                to_position = key
                break

        self.numbers[from_position], self.numbers[to_position] = self.numbers[to_position], self.numbers[from_position]
        self.current_position = to_position

    def game_over(self):
        is_game_over = False
        for key, value in self.numbers.items():
            if value == '':
                pass
            else:
                if key == value:
                    is_game_over = True
                else:
                    is_game_over = False
        return is_game_over

    def star_game(self):
        self.make_board()
        self.print_board()
        print('0 -- выход')

        while True:
            print('Доступные ячейки для перемещения: ')
            free_cells = self.valid_moves()
            temp_list = []
            for i in free_cells:
                temp_list.append(i)
                print(i, ' ', end='')

            print()
            print('Введите номер ячейки: ')

            x = int(input())

            if x == 0:
                break
            elif x not in temp_list:
                print('Выбрана неверная ячейка')
            else:
                self.change(x)

            self.print_board()

            if self.game_over():
                print('Победа!')
                break
