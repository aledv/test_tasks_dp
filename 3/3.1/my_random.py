# -*- coding: utf8 -*-

import random


def my_random():
    """
    Receives a random number from 1 to 7
    :return:
    """
    i = 0
    j = 7

    while j >= 0:
        i += random.randrange(1, 5)
        j -= 1

    return i % 7 + 1
