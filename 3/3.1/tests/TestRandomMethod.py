import unittest

import math

from my_random import my_random


class TestRandomMethod(unittest.TestCase):

    def test_random_interval(self):
        min = 1
        max = 7

        for _ in range(1, 1000):
            random_number = my_random()
            self.assertTrue(random_number >= min and random_number <= max)

    def test_random_distribution(self):
        values = {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
            7: 0,
        }
        attempts = 1000
        equal_distributions = math.ceil(attempts / 7)
        epsilon = math.ceil((equal_distributions / 100) * 10)

        for _ in range(1, attempts):
            random_number = my_random()
            values[random_number] += 1

        for key, value in values.items():
            self.assertTrue(value <= equal_distributions + epsilon)


if __name__ == '__main__':
    unittest.main()
