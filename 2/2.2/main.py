# -*- coding: utf-8 -*-

import numpy


def main():
    matrix_rotation_method_1(4)
    matrix_rotation_method_2(4)


def matrix_rotation_method_1(size: int):
    matrix = _generate_matrix(size)
    print('Input\n', matrix)

    rotated_matrix = [[matrix[j][i] for j in range(len(matrix))] for i in range(len(matrix[0]) - 1, -1, -1)]
    print('Output\n', rotated_matrix)

    return rotated_matrix


def matrix_rotation_method_2(size: int):
    matrix = _generate_matrix(size)
    print('Input\n', matrix)

    rotated_matrix = numpy.rot90(matrix).tolist()
    print('Output\n', rotated_matrix)

    return rotated_matrix


def _generate_matrix(size: int):
    elements = list(range(1, pow(size, 2) + 1))
    matrix = [elements[i:i + size] for i in range(0, len(elements), size)]

    return matrix


if __name__ == "__main__":
    main()
