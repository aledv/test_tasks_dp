# -*- coding: utf8 -*-

import requests


class RemoteApi:
    """
    Список методов и параметров: http://jsonplaceholder.typicode.com/
    """

    def __init__(self):
        self.endpoint = 'https://jsonplaceholder.typicode.com'

    def get_posts(self):
        endpoint = self.endpoint + '/posts'
        posts = requests.get(endpoint).json()

        return posts

    def get_post_by_id(self, id: int):
        endpoint = self.endpoint + '/posts/' + str(id)
        post = requests.get(endpoint).json()

        return post

    def get_users(self):
        endpoint = self.endpoint + '/users'
        users = requests.get(endpoint).json()

        return users

    def get_user_by_id(self, id: int):
        endpoint = self.endpoint + '/users/' + str(id)
        user = requests.get(endpoint).json()

        return user
