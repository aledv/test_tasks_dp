import unittest

from models.RemoteApi import RemoteApi


class TestRemoteApiPosts(unittest.TestCase):

    def setUp(self) -> None:
        self.api = RemoteApi()

    def test_get_users(self):
        posts = self.api.get_posts()
        self.assertEqual(len(posts), 100)

    def test_get_post_by_right_id(self):
        post = self.api.get_post_by_id(15)
        self.assertEqual(post['id'], 15)

    def test_get_post_by_wrong_id(self):
        post = self.api.get_post_by_id(1000)
        self.assertEqual(post, {})


if __name__ == '__main__':
    unittest.main()
