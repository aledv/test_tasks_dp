import unittest

from models.RemoteApi import RemoteApi


class TestRemoteApiUsers(unittest.TestCase):

    def setUp(self) -> None:
        self.api = RemoteApi()

    def test_get_users(self):
        users = self.api.get_users()
        self.assertEqual(len(users), 10)

    def test_get_user_by_right_id(self):
        user = self.api.get_user_by_id(5)
        self.assertEqual(user['id'], 5)

    def test_get_user_by_wrong_id(self):
        user = self.api.get_user_by_id(11)
        self.assertEqual(user, {})


if __name__ == '__main__':
    unittest.main()
