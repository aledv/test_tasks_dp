# -*- coding: utf8 -*-

from models.RemoteApi import RemoteApi


def main():
    remote_api = RemoteApi()
    posts = remote_api.get_posts()
    print(posts)

    post = remote_api.get_post_by_id(101)
    print(post)

    user = remote_api.get_user_by_id(101)
    print(user)


if __name__ == '__main__':
    main()
