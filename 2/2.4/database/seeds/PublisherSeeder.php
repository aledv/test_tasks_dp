<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PublisherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 3) as $i) {
            $sql = 'INSERT INTO publishers (name) VALUES (:name)';
            DB::insert($sql, [
                'name' => 'Publisher #' . $i,
            ]);
        }
    }
}
