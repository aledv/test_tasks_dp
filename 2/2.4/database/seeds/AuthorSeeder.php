<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 9) as $i) {
            $sql = 'INSERT INTO authors (name) VALUES (:name)';
            DB::insert($sql, [
                'name' => "Author #" . $i,
            ]);
        }
    }
}
