<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RelationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->authorPublisher();
        $this->bookAuthor();
    }

    private function authorPublisher()
    {
        $publishers = DB::select('SELECT * FROM publishers');
        $authors = DB::select('SELECT * FROM authors WHERE id NOT IN (2, 5, 7)');

        foreach ($publishers as $i => $publisher) {
            if ($publisher->id === 1) { continue; }

            $authorsPart = array_slice($authors, $i, 3);

            foreach ($authorsPart as $author) {

                $sql = 'INSERT INTO author_publisher (publisher_id, author_id) VALUES (:pub_id, :author_id)';
                DB::insert($sql, [
                    'pub_id' => $publisher->id,
                    'author_id' => $author->id,
                ]);
            }
        }
    }

    private function bookAuthor()
    {
        $books = DB::select('SELECT * FROM books');
        $authors = DB::select('SELECT * FROM authors');

        foreach ($authors as $i => $author) {
            $booksPart = array_slice($books, $i, 2);

            foreach ($booksPart as $book) {
                $sql = 'INSERT INTO book_author (book_id, author_id) VALUES (:book_id, :author_id)';
                DB::insert($sql, [
                    'book_id' => $book->id,
                    'author_id' => $author->id,
                ]);
            }
        }
    }
}
