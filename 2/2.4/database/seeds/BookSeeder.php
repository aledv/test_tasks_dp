<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 18) as $i) {
            $sql = 'INSERT INTO books (title) VALUES (:title)';
            DB::insert($sql, [
                'title' => "Book #" . $i,
            ]);
        }
    }
}
