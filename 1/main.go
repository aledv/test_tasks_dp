package main

import (
	"fmt"
	"strconv"
	"strings"
)

func check_commission(number string) uint64 {
	tempValue, err := strconv.ParseFloat(number, 64)
	if err != nil {
		fmt.Println("Введено не число:", number)
		return 0
	}

	if tempValue < 0 {
		fmt.Println("Комиссия не может быть отрицательной:", number)
		return 0
	}

	if tempValue > 99.99 {
		fmt.Println("Комиссия не может быть больше 100:", number)
		return 0
	}

	result := preapre_comission_value(number)

	return result
}

func preapre_comission_value(number string) uint64 {

	if !strings.Contains(number, ".") {
		fmt.Println(number)

		number, err := strconv.ParseInt(number, 10, 64)
		if err != nil {
			return 0
		}

		return uint64(number) * 100
	}

	numberParts := strings.Split(number, ".")
	integer := numberParts[0]
	fraction := numberParts[1]

	if len(fraction) > 2 {
		fraction = fraction[:2]
	} else if len(fraction) == 1 {
		fraction = fraction[:1] + "0"
	}

	if integer == "0" {
		tempValue, err := strconv.ParseUint(fraction, 10, 64)
		if err == nil {
			return tempValue
		}
	} else {
		tempValue, err := strconv.ParseUint(integer+fraction, 10, 64)
		if err == nil {
			return tempValue
		}
	}

	return 0
}

func main() {
	input := "неЧисло"
	fmt.Println(input, " -> ", check_commission(input))

	input = "1"
	fmt.Println(input, " -> ", check_commission(input))

	input = "100"
	fmt.Println(input, " -> ", check_commission(input))

	input = "-10"
	fmt.Println(input, " -> ", check_commission(input))

	for i := 0.01; i < 100; i += 0.01 {
		input := fmt.Sprintf("%.2f", i)
		fmt.Println(input, " -> ", check_commission(input))
	}
}
